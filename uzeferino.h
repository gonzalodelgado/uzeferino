/*
 * Copyright (c) 2016 Gonzalo Delgado
 *
 * Based upon "Don Ceferino Hazaña" by Hugo Ruscitti,
 * Walter Velazquez, Javier Da Silva, and José Jorge Enríquez Rodríguez
 *
 * This file is part of Uzeferino.
 *
 * Uzeferino is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Uzeferino is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Uzeferino. If not, see <http://www.gnu.org/licenses/>.
 */

#define GRAVITY 1
#define BALLSPEED 1
#define PLAYERSPEED 2
#define KNIFESPEED 4
#define SCREEN_WIDTH 240
#define SCREEN_HEIGHT (SCREEN_TILES_H << 3)     // multiplies by 8 == TILE_HEIGHT
#define SCREEN_TILES_WIDTH 240/TILE_WIDTH
#define SCREEN_TILES_HEIGHT 224/TILE_HEIGHT
#define BALL_TILE_WIDTH BALLSPRITE_WIDTH*TILE_WIDTH
#define BALL_TILE_HEIGHT BALLSPRITE_HEIGHT*TILE_HEIGHT
#define PLAYER_TILE_HEIGHT PLAYERSPRITE_HEIGHT*TILE_HEIGHT
#define MAXPLAYERX SCREEN_WIDTH - PLAYERSPRITE_WIDTH * TILE_WIDTH
#define K_SHOT 1

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0])) 

typedef struct {
        u8 now, last;
} InputState;

typedef struct {
        u8 sprite,
           x, y,
           width, height;
} Actor;

typedef enum {ballXS = 1, ballS = 2, ballM = 3, ballL = 4} BallSize;

typedef struct {
        u8 x, y;
        BallSize size;
} LevelBall;

typedef struct {
        u8 sprite;
        LevelBall levelData;
        s8 velx, vely;
        char* spriteMap;
} Ball;

typedef enum {idle, active} KnifeState;

typedef struct {
        Actor actor;
        KnifeState state;
} Knife;

typedef struct {
        u8 spriteIndex;
        u8 level;
        // player lives, score, etc.
} GameState;
