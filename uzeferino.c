/*
 * Copyright (c) 2016 Gonzalo Delgado
 *
 * Based upon "Don Ceferino Hazaña" by Hugo Ruscitti,
 * Walter Velazquez, Javier Da Silva, and José Jorge Enríquez Rodríguez
 *
 * This file is part of Uzeferino.
 *
 * Uzeferino is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Uzeferino is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Uzeferino. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <avr/io.h>
#include <stdlib.h>
#include <avr/pgmspace.h>
#include <uzebox.h>
#include "uzeferino.h"

#include "data/levels.inc"
#include "data/sprites.inc"
#include "data/tiles.inc"


static InputState GetInputState(InputState curr_state) {
        InputState result = {};
        result.last = curr_state.now;
        result.now = ReadJoypad(0);
        return result;
}

static void RenderSprite(u8 startSprite, const char *spriteMap, u8 x, u8 y, u8 width, u8 height) {
        MapSprite(startSprite, spriteMap);
        MoveSprite(startSprite, x, y, width, height);
}

static Actor UpdatePlayerPos(Actor player, InputState input_st) {
        Actor result = {};
        result.sprite = player.sprite;
        result.x = player.x;
        result.y = player.y;
        result.width = player.width;
        result.height = player.height;
        if (input_st.now & BTN_LEFT) {
                result.x -= PLAYERSPEED;
                if (result.x < 0) {
                        result.x = 0;
                }
        }
        if (input_st.now & BTN_RIGHT) {
                result.x += PLAYERSPEED;
                if (MAXPLAYERX < result.x) {
                        result.x = MAXPLAYERX;
                }
        }
        return result;
}

static Ball UpdateBallPos(Ball b) {
        b.vely += GRAVITY;
        b.levelData.y += b.vely;
        b.levelData.x += b.velx;
        u8 maxBallY = SCREEN_HEIGHT - b.levelData.size*TILE_HEIGHT;
        
        if(b.levelData.y >= maxBallY) {
                b.vely = -18;      // what's the physics name of this? bounce??
                b.levelData.y = maxBallY;
        }
        if(b.levelData.x <= 0 || SCREEN_WIDTH <= b.levelData.x + b.levelData.size*TILE_WIDTH) {
                b.velx *= -1;
                if (SCREEN_WIDTH < b.levelData.x + b.levelData.size*TILE_WIDTH) { //TODO: define and use MAXBALLX for this
                        b.levelData.x = SCREEN_WIDTH - b.levelData.size*TILE_WIDTH;
                } else if (b.levelData.x < 0) {
                        b.levelData.x = 0;
                }
        }
        return b;
}

static Knife HandleKnifeThrow(Knife knife, Actor player, InputState input_st) {
        Knife result = {};
        result.actor.sprite = knife.actor.sprite;
        result.actor.width = knife.actor.width;
        result.actor.height = knife.actor.height;
        if (knife.state == active) {
                result.actor.x = knife.actor.x;
                result.actor.y = knife.actor.y - KNIFESPEED;
                if (result.actor.y <= 1) {
                        result.state = idle;
                        result.actor.x = OFF_SCREEN;
                        result.actor.y = 0;
                } else {
                        result.state = active;
                }
        } else if (input_st.now & BTN_UP) {
                result.actor.x = player.x;
                result.actor.y = player.y - player.height*TILE_HEIGHT;
                result.state = active;
        } else {
                result.actor.x = OFF_SCREEN;
                result.actor.y = 0;
        }
        return result;
}

u8 getNextSpriteIndex(GameState* gameState, u8 width, u8 height) {
        u8 result = gameState->spriteIndex;
        gameState->spriteIndex = gameState->spriteIndex + width + height + 1;
        return result;
}

Actor InitActor(u8 x, u8 y, u8 width, u8 height, GameState* gameState) {
        Actor result = {};
        result.x = x;
        result.y = y;
        result.width = width;
        result.height = height;
        result.sprite = getNextSpriteIndex(gameState, result.width, result.height);
        return result;
}

GameState InitGameState() {
        GameState result = {};
        return result;
}

// TODO: move this into a LoadLevel function or something
void InitBalls(Ball balls[], u8 nballs, GameState* gameState) {
        for (u8 i=0; i<nballs; i++) {
                memcpy_P(&(balls[i].levelData),
                         (levelBalls + i),
                         sizeof(LevelBall));
                balls[i].sprite = getNextSpriteIndex(gameState, (u8)balls[i].levelData.size, (u8)balls[i].levelData.size);
                balls[i].velx = 1;
                balls[i].vely = 1;
                switch (balls[i].levelData.size) {
                        case ballXS:
                                balls[i].spriteMap = ballXSSprite;
                                break;
                        case ballS:
                                balls[i].spriteMap = ballSSprite;
                                break;
                        case ballM:
                                balls[i].spriteMap = ballMSprite;
                                break;
                        /*case ballL:*/
                                /*balls[i].spriteMap = ballLSprite;*/
                                /*break;*/
                        /*default:*/
                                 /*what to do here? remove ball? bail out?*/
                }
        }
}

int main() {
        bool pause = false;
        GameState gameState = InitGameState();
        Actor player = InitActor(0, SCREEN_HEIGHT - PLAYER_TILE_HEIGHT*2, PLAYERSPRITE_WIDTH, PLAYERSPRITE_HEIGHT, &gameState);
        Ball balls[NBALLS];
        u8 i;
        InputState input_st = {};

        SetTileTable(tile_data);
        SetSpritesTileTable(sprites_tile_data);
        ClearVram();
        SetSpriteVisibility(true);
        
        InitBalls(balls, NBALLS, &gameState);
        Knife knife = {};
        knife.actor = InitActor(0, 0, KNIFESPRITE_WIDTH, KNIFESPRITE_HEIGHT, &gameState);
        knife.state = idle;

        while(1) {
                if (GetVsyncFlag()) {
                        ClearVsyncFlag();
                        input_st = GetInputState(input_st);
                        //BAAAAAD pause code
                        if (input_st.now & BTN_START) {
                                pause = !pause;
                        }
                        if (pause) {
                                continue;
                        }
                        player = UpdatePlayerPos(player, input_st);
                        RenderSprite(player.sprite, playerSprite, player.x, player.y, player.width, player.height);
                        knife = HandleKnifeThrow(knife, player, input_st);
                        RenderSprite(knife.actor.sprite, knifeSprite, knife.actor.x, knife.actor.y, knife.actor.width, knife.actor.height);
                        for (i=0; i<ArrayCount(balls); i++) {
                                balls[i] = UpdateBallPos(balls[i]);
                                RenderSprite(
                                        balls[i].sprite,
                                        balls[i].spriteMap,
                                        balls[i].levelData.x,
                                        balls[i].levelData.y,
                                        (u8)balls[i].levelData.size,
                                        (u8)balls[i].levelData.size);
                        }
                }
        }

        return 0;
}
