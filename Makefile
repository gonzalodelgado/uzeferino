###############################################################################
# Makefile for Uzeferino
###############################################################################

## General Flags
PROJECT = uzeferino
GAME = uzeferino
MCU = atmega644
BUILDDIR = ./build
TARGET = $(GAME).elf
CC = avr-gcc
INFO=gameinfo.properties

## Kernel settings
#KERNEL_DIR = /path/to/uzebox/kernel	<-- this will be picked up from an environment variable
KERNEL_OPTIONS  = -DVIDEO_MODE=3 -DINTRO_LOGO=0 -DSCROLLING=0
KERNEL_OPTIONS += -DMAX_SPRITES=24 -DRAM_TILES_COUNT=32


## Options common to compile, link and assembly rules
COMMON = -mmcu=$(MCU)

## Compile options common for all C compilation units.
CFLAGS = $(COMMON)
CFLAGS += -Wall -gdwarf-2 -std=gnu99 -DF_CPU=28636360UL -O0 -fsigned-char -ffunction-sections -fno-toplevel-reorder
CFLAGS += -MD -MP -MT $(*F).o -MF dep/$(@F).d 
CFLAGS += $(KERNEL_OPTIONS)
CFLAGS += -g


## Assembly specific flags
ASMFLAGS = $(COMMON)
ASMFLAGS += $(CFLAGS)
ASMFLAGS += -x assembler-with-cpp -Wa,-gdwarf2

## Linker flags
LDFLAGS = $(COMMON)
LDFLAGS += -Wl,-Map=$(BUILDDIR)/$(GAME).map 
LDFLAGS += -Wl,-gc-sections 

## Intel Hex file production flags
HEX_FLASH_FLAGS = -R .eeprom

HEX_EEPROM_FLAGS = -j .eeprom
HEX_EEPROM_FLAGS += --set-section-flags=.eeprom="alloc,load"
HEX_EEPROM_FLAGS += --change-section-lma .eeprom=0 --no-change-warnings


## Objects that must be built in order to link
OBJECTS = $(BUILDDIR)/uzeboxVideoEngineCore.o $(BUILDDIR)/uzeboxCore.o $(BUILDDIR)/uzeboxSoundEngine.o $(BUILDDIR)/uzeboxSoundEngineCore.o $(BUILDDIR)/uzeboxVideoEngine.o $(BUILDDIR)/$(GAME).o 

## Objects explicitly added by the user
LINKONLYOBJECTS = 

## Include Directories
INCLUDES = -I"$(KERNEL_DIR)" 

## Build
all: clean build
build: data/sprites.inc $(TARGET) $(GAME).hex $(GAME).eep $(GAME).lss $(GAME).uze size

## (re)generate graphics include file
data/sprites.inc: art/sprites.png sprites.gconvert.xml
	gconvert sprites.gconvert.xml

## Compile Kernel files
$(BUILDDIR)/uzeboxVideoEngineCore.o: $(KERNEL_DIR)/uzeboxVideoEngineCore.s
	$(CC) $(INCLUDES) $(ASMFLAGS) -c -o $(BUILDDIR)/uzeboxVideoEngineCore.o $<

$(BUILDDIR)/uzeboxSoundEngineCore.o: $(KERNEL_DIR)/uzeboxSoundEngineCore.s
	$(CC) $(INCLUDES) $(ASMFLAGS) -c -o $(BUILDDIR)/uzeboxSoundEngineCore.o $<

$(BUILDDIR)/uzeboxCore.o: $(KERNEL_DIR)/uzeboxCore.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  -o $(BUILDDIR)/uzeboxCore.o $<

$(BUILDDIR)/uzeboxSoundEngine.o: $(KERNEL_DIR)/uzeboxSoundEngine.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  -o $(BUILDDIR)/uzeboxSoundEngine.o $<

$(BUILDDIR)/uzeboxVideoEngine.o: $(KERNEL_DIR)/uzeboxVideoEngine.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  -o $(BUILDDIR)/uzeboxVideoEngine.o $<

## Compile game sources
$(BUILDDIR)/$(GAME).o: $(GAME).c
	$(CC) $(INCLUDES) $(CFLAGS) -c -o $(BUILDDIR)/$(GAME).o $<

##Link
$(TARGET): $(OBJECTS)
	 $(CC) $(LDFLAGS) $(OBJECTS) $(LINKONLYOBJECTS) $(LIBDIRS) $(LIBS) -o $(BUILDDIR)/$(TARGET)

%.hex: $(TARGET)
	avr-objcopy -O ihex $(HEX_FLASH_FLAGS) $(BUILDDIR)/$< $(BUILDDIR)/$@

%.eep: $(TARGET)
	-avr-objcopy $(HEX_EEPROM_FLAGS) -O ihex $(BUILDDIR)/$< $(BUILDDIR)/$@ || exit 0

%.lss: $(TARGET)
	avr-objdump -h -S $(BUILDDIR)/$< > $(BUILDDIR)/$@

%.uze: $(TARGET)
	-packrom $(BUILDDIR)/$(GAME).hex $(BUILDDIR)/$@ $(INFO)

UNAME := $(shell sh -c 'uname -s 2>/dev/null || echo not')
AVRSIZEFLAGS := -A ${BUILDDIR}/${TARGET}
ifneq (,$(findstring MINGW,$(UNAME)))
AVRSIZEFLAGS := -C --mcu=${MCU} ${BUILDDIR}/${TARGET}
endif

size: ${TARGET}
	@echo
	@avr-size ${AVRSIZEFLAGS}

## Clean target
.PHONY: clean build all
clean:
	-rm -rf $(BUILDDIR)/* dep/* data/sprites.inc


## Other dependencies
-include $(shell mkdir dep 2>/dev/null) $(wildcard dep/*)

