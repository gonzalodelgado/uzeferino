Uzeferino
=========

An attempt to clone [Don Ceferino Hazaña](http://losersjuegos.com.ar/juegos/ceferino) (Pang/Buster Bros. clone by [Losers Juegos](http://losersjuegos.com.ar/)) on the [Uzebox](https://uzebox.org/).

Quickstart
----------

* To get this working you need avr-gcc and uzebox. See the [Uzebox wiki to get started](http://uzebox.org/wiki/index.php?title=Getting_Started_on_the_Uzebox#Software_Installation)
* Download uzebox code from https://github.com/Uzebox/uzebox/ into your filesystem
* Compile: `KERNEL_DIR=/path/to/uzebox/kernel make`
* Run: `uzem -s build build/uzeferino.hex`
